<?php
class Config
{
	private static $DB_server = "gipsart.develop";
	private static $DB_name = "gipsart";
	private static $DB_username = "root";
	private static $DB_password = "";
	private static $url = "http://gipsart.develop/public";
	
	public function GetParamDb()
	{
		return 	$params = [
			'DB_server'		=> self::$DB_server,
			'DB_name'  		=> self::$DB_name,
			'DB_username'  	=> self::$DB_username,
			'DB_password'  => self::$DB_password,
		];
	}
	public function url()
	{
		return 	self::$url;
	}
}

?>