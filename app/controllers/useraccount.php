<?php
class UserAccount extends Controller
{
	public function index($name = '')
	{
		$user = $this->model('useraccount');
		$user->name = $name;
		
		$this->view('useraccount/useraccount', ['name'=>$user->name]);
		
	}		
}

?>